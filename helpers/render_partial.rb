# this partial has borrowed code from:
# http://github.com/adamstac/sinatra-bootstrap/blob/master/lib/render_partial.rb
# http://github.com/cschneid/irclogger/blob/master/lib/partials.rb
# http://gist.github.com/119874

module Sinatra
  module RenderPartial
    # Usage: partial :foo
    # renders the page once
    # Usage: partial :foo, :collection => @my_foos
    # foo will be rendered once for each element in the array
    def partial(template, *args)
      template_array = template.to_s.split('/')
      template = template_array[0..-2].join('/') + "/#{template_array[-1]}" # no underscore
      # a fixed views/partials starting point:
      template = "partials/#{template}".intern
      options = args.last.is_a?(Hash) ? args.pop : {}
      options.merge!(:layout => false)
      if collection = options.delete(:collection) then
        collection.inject([]) do |buffer, member|
          buffer << haml(:"#{template}", options.merge(:layout =>
          false, :locals => {template_array[-1].to_sym => member}))
        end.join("\n")
      else
        haml(template, options)
      end
    end
  end
 
  helpers RenderPartial
end