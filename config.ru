require 'app'

use Rack::Static,
  :root => "public",
  :urls => ["/images",
            "/javascripts"]

run FluidmixApp
