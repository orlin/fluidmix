require 'rubygems'
require 'sinatra/base'
require 'haml/util'
require 'haml/engine'
require 'compass'
require 'helpers/render_partial'

class FluidmixApp < Sinatra::Default

  set :root, File.dirname(__FILE__)

  configure :production do
    enable :raise_errors
    disable :show_exceptions
    not_found do
      @view_title = "Sorry"
      @bummer = "Our apologies, but we don't know what this is."
      haml :sorry
    end
    error do
      @view_title = "Bummer"
      @bummer = "What a bummer! We're right on it!"
      haml :sorry
    end
  end

  configure do
    Compass.configuration.parse(File.join(self.root, 'config', 'compass.rb'))
    set :haml, { :format => :html5 }
    set :sass, Compass.sass_engine_options
  end

  get '/stylesheets/:name.css' do
    content_type 'text/css', :charset => 'utf-8'
    sass(:"stylesheets/#{params[:name]}", Compass.sass_engine_options)
  end

  before do
    headers "Content-Type" => "text/html; charset=utf-8"
  end

  get '/' do
    @uber_title = "Welcome to Fluidmix"
    @custom_style = "index"
    haml :index
  end

end
